class UnionFind:
	vertices = []
	edges = []
	clusters = 0
	def __init__(self,vertices, edges,clusters):
		self.vertices = vertices
		self.edges = edges
		self.clusters = clusters
	def find(self,vertex):
		return vertex.groupPtr
	def union(self,group1, group2):
		group1Arr = [x for x in self.vertices if x.groupPtr == group1]
		group2Arr = [x for x in self.vertices if x.groupPtr == group2]
		if (len(group1Arr) >= len(group2Arr)):
			for y in range(0,len(group2Arr)):
				self.vertices[group2Arr[y].ptr-1].groupPtr = group1
		elif (len(group1Arr) < len(group2Arr)):
			for y in range(0,len(group1Arr)):
				self.vertices[group1Arr[y].ptr-1].groupPtr = group2
		self.clusters -= 1

#UnionFind class that stores nodes as a 24 bit string
class BitUnionFind:
	nodes = {}
	clusterCount = 0
	clusters = {}
	def __init__(self,clusters, nodes):
		self.clusterCount = clusters
		self.nodes = nodes
		for k,v in nodes.iteritems():
			self.clusters[str(v)] = [k]
	def find(self,vertex):
		return self.nodes[vertex]
	def union(self,group1, group2):
		value1 = str(self.nodes[group1])
		value2 = str(self.nodes[group2])

		group1Arr = self.clusters[value1]
		group2Arr = self.clusters[value2]

		self.clusterCount -= 1

		if (len(group1Arr) >= len(group2Arr)):
			for x in group2Arr:
				self.nodes[x] = int(value1)
				self.clusters[value1].append(x)
				if len(self.clusters[value2]) == 0:
					del self.clusters[value2]
		elif (len(group1Arr) < len(group2Arr)):
			for y in group1Arr:
				self.nodes[y] = int(value2)
				self.clusters[value2].append(y)
				if len(self.clusters[value1]) == 0:
					del self.clusters[value1]


class edge:
	vertex1 = -1
	vertex2 = -1
	cost = -1

	def __init__(self,vertex1,vertex2,cost):
		self.vertex1 = vertex(vertex1)
		self.vertex2 = vertex(vertex2)
		self.cost = cost

class vertex:
	ptr = -1
	groupPtr = -1 #Python doesn't have pointers, so simulate one with this variable
	def __init__(self,ptr):
		self.ptr = ptr
		self.groupPtr = ptr

'''In this programming problem and the next you'll code up the clustering algorithm from lecture for computing a max-spacing k-clustering. Download the text file here. This file describes a distance function (equivalently, a complete graph with edge costs). It has the following format:
[number_of_nodes]
[edge 1 node 1] [edge 1 node 2] [edge 1 cost]
[edge 2 node 1] [edge 2 node 2] [edge 2 cost]
...
There is one edge (i,j) for each choice of 1<=i<j<=n, where n is the number of nodes. For example, the third line of the file is "1 3 5250", indicating that the distance between nodes 1 and 3 (equivalently, the cost of the edge (1,3)) is 5250. You can assume that distances are positive, but you should NOT assume that they are distinct.

Your task in this problem is to run the clustering algorithm from lecture on this data set, where the target number k of clusters is set to 4. What is the maximum spacing of a 4-clustering?'''

## Problem 1

## returns 106
def problem1():
	minCost = 0
	unionFind = UnionFind([],[],500)
	f = open('clustering1.txt', 'r')
	#Read the input and store each edge in the unionFind
	for line in f:
		arr = line.split()
		if len(arr) == 3:
			unionFind.edges.append(edge(int(arr[0]),int(arr[1]),int(arr[2])))

	#Sort the edges in order of increasing cost
	unionFind.edges.sort(key=lambda x: x.cost, reverse=False)

	#Add vertices index 1 to 500 to the unionFind
	for x in range(1,501):
		unionFind.vertices.append(vertex(x))

	#Run k-clustering until there are four clusters left
	while unionFind.clusters > 3:
		#Get the next edge in the graph
		nextEdge = unionFind.edges[0]

		#Each vertex the edge connects
		nextEdge.vertex1 = unionFind.vertices[nextEdge.vertex1.ptr-1]
		nextEdge.vertex2 = unionFind.vertices[nextEdge.vertex2.ptr-1]

		#If the two vertices aren't currently in the same cluster
		if unionFind.find(nextEdge.vertex1) != unionFind.find(nextEdge.vertex2):
			if unionFind.clusters == 4:
				print unionFind.edges[0].cost
				break
			#Join the two edges into a cluster
			unionFind.union(nextEdge.vertex1.groupPtr, nextEdge.vertex2.groupPtr)
		else:
			#Keep track of the minimum cost
			minCost = nextEdge.cost
		#remove the edge that was just considered
		unionFind.edges.remove(unionFind.edges[0])
problem1()


'''In this question your task is again to run the clustering algorithm from lecture, but on a MUCH bigger graph. So big, in fact, that the distances (i.e., edge costs) are only defined implicitly, rather than being provided as an explicit list.
The data set is here. The format is:
[# of nodes] [# of bits for each node's label]
[first bit of node 1] ... [last bit of node 1]
[first bit of node 2] ... [last bit of node 2]
...
For example, the third line of the file "0 1 1 0 0 1 1 0 0 1 0 1 1 1 1 1 1 0 1 0 1 1 0 1" denotes the 24 bits associated with node #2.

The distance between two nodes u and v in this problem is defined as the Hamming distance--- the number of differing bits --- between the two nodes' labels. For example, the Hamming distance between the 24-bit label of node #2 above and the label "0 1 0 0 0 1 0 0 0 1 0 1 1 1 1 1 1 0 1 0 0 1 0 1" is 3 (since they differ in the 3rd, 7th, and 21st bits).

The question is: what is the largest value of k such that there is a k-clustering with spacing at least 3? That is, how many clusters are needed to ensure that no pair of nodes with all but 2 bits in common get split into different clusters?'''


#Function that flips at bit, with a string as an input
#Could be done more efficiently with bitwise operators
def flip(bit):
	if bit == "0":
		return "1"
	return "0"

#Generator function that return the next permutation of the input string with one or two bits flipped
#Returns -1 when it has exhausted all possibilities
def generateFlips(node):
	oldNode = node
	node = list(node)
	#First do flips with one bit different
	for x in range(0,len(node)):
		node[x] = flip(node[x])
		if x > 0:
			node[x-1] = flip(node[x-1])
		yield "".join(node)
	node = list(oldNode)

	#Now with two bits different
	for x in range(0,len(node)):
		node[x] = flip(node[x])
		for y in range(x+1, len(node)):
			node[y] = flip(node[y])
			if (y - x > 1):
				node[y-1] = flip(node[y-1])
			yield "".join(node)
		node[x] = flip(node[x])
		node[len(node) - 1] = flip(node[len(node) - 1])
	yield -1  


#returns 6118
#The idea here is to find all of the points that are within Hamming distance of 3 of each point in the original list and union them in the UnionFind
#Do this by generating all of the possibilities and checking whether they are in the original graph
#This is far more efficient than explicity defining edges when the graph is this large
#The answer is the number of clusters at the end
def problem2():
	unexploredNodes = {}
	exploredNodes = {}

	f = open('clustering_big.txt', 'r')
	lineNum = 1
	#Parse the input, adding each node the the unexplored list
	for line in f:
		if len(line) > 10:
			unexploredNodes[(line.strip()).replace(" ","")] = lineNum
			lineNum += 1 
	#Create the unionFind, this time using the one that stores the nodes as a bit string
	unionFind = BitUnionFind(len(unexploredNodes), unexploredNodes)
	
	#Progess tracking variable
	count = 0

	#Iterate through each vertex in the input 
	for key,value in unionFind.nodes.iteritems():
		if count % 10000 == 0:
			print count
		count += 1
		#Get the next permutation
		flips = generateFlips(key)
		nextTest = flips.next()

		#While the combinations for the current node haven't been exhausted
		while (nextTest != -1):
			#If the generated node is in the original list
			if nextTest in unionFind.nodes: 
				#If the arent't the same cluster join them
				if unionFind.find(key) != unionFind.find(nextTest):
					unionFind.union(key, nextTest)

			#Get the next permutation
			nextTest = flips.next()

	#Print the current number of clusters, which is the answer
	print unionFind.clusterCount
problem2()












